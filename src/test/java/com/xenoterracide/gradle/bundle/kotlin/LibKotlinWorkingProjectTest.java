/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.bundle.kotlin;

import org.gradle.internal.impldep.org.junit.Rule;
import org.gradle.internal.impldep.org.junit.rules.TemporaryFolder;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.gradle.util.GFileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class LibKotlinWorkingProjectTest {
    private GradleRunner project;
    @Rule
    private TemporaryFolder testProjectDir = new TemporaryFolder();
    private File tmp;

    @BeforeEach
    void setup() throws Exception {
        testProjectDir.create();
        File working = Paths.get( "tp/lib/works/kotlin" ).toFile();
        tmp = testProjectDir.newFolder();

        GFileUtils.copyDirectory( working, tmp );
    }


    @Test
    void ongoing() throws IOException {
        project = GradleRunner.create().withProjectDir( tmp ).withPluginClasspath();

        BuildResult build = project.withArguments( "build", "--stacktrace", "--info" ).build();
        String output = build.getOutput();
        LoggerFactory.getLogger( this.getClass() ).info( build::getOutput );
        assertThat( output ).contains( "BUILD SUCCESSFUL" );

        Path libs = project.getProjectDir().toPath().resolve( "build/libs" );
        try ( Stream<Path> jars = Files.find(
                libs,
                1,
                ( path, basicFileAttributes ) -> path.getFileName().toFile().getName().matches( "working.*\\.jar" )
        ) ) {
            assertThat( jars ).hasSize( 1 );
        }
    }

}
