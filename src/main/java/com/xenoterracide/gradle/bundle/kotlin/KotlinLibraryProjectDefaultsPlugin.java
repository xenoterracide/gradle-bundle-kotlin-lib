/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.bundle.kotlin;

import com.xenoterracide.gradle.bom.DependencyManagement;
import com.xenoterracide.gradle.idea.IntelliJDefaultsPlugin;
import com.xenoterracide.gradle.junit.JunitDefaultsPlugin;
import com.xenoterracide.gradle.kotlin.KotlinDefaultsPlugin;
import com.xenoterracide.gradle.mirror.MirrorPlugin;
import com.xenoterracide.gradle.semver.SemVerPlugin;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.MavenPom;
import org.gradle.api.publish.maven.MavenPublication;
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.jvm.tasks.Jar;
import org.jetbrains.dokka.gradle.DokkaPlugin;
import org.jetbrains.dokka.gradle.DokkaTask;

import java.util.Arrays;

public class KotlinLibraryProjectDefaultsPlugin implements Plugin<Project> {
    @Override
    public void apply( Project project ) {

        Arrays.asList(
                DokkaPlugin.class,
                MavenPublishPlugin.class,
                IntelliJDefaultsPlugin.class,
                JunitDefaultsPlugin.class,
                KotlinDefaultsPlugin.class,

                DependencyManagement.class,
                SemVerPlugin.class,
                MirrorPlugin.class
        ).forEach( plugin -> project.getPluginManager().apply( plugin ) );

        String testImpl = "testImplementation";
        DependencyHandler deps = project.getDependencies();
        deps.add( testImpl, "org.junit.jupiter:junit-jupiter-api" );
        deps.add( testImpl, "org.junit.jupiter:junit-jupiter-params" );
        deps.add( "testRuntimeOnly", "org.junit.jupiter:junit-jupiter-engine" );
        deps.add( testImpl, "org.assertj:assertj-core" );

        project.getExtensions().configure( PublishingExtension.class, ext -> {
            TaskContainer tasks = project.getTasks();
            ext.publications( pubs -> {
                TaskProvider<Jar> sources = tasks.register( "sourcesJar", Jar.class, jar -> {
                    jar.setClassifier( "sources" );
                    jar.from( project.getExtensions()
                            .getByType( SourceSetContainer.class )
                            .getByName( SourceSet.MAIN_SOURCE_SET_NAME )
                            .getAllSource()
                    );
                } );
                tasks.withType( DokkaTask.class ).configureEach( task -> {
                    task.setOutputFormat( "html" );
                    task.setOutputDirectory( "$buildDir/javadoc" );
                    task.setJdkVersion( 8 );
                } );
                TaskProvider<Jar> javadocJar = tasks.register( "javadocJar", Jar.class, jar -> {
                    jar.setClassifier( "javadoc" );
                    jar.setGroup( JavaBasePlugin.DOCUMENTATION_GROUP );
                    jar.from( tasks.getByName( "dokka" ) );
                } );
                pubs.create( "mavenJava", MavenPublication.class, pub -> {
                    MavenPom pom = pub.getPom();
                    pom.developers( devs -> {
                        devs.developer( dev -> {
                            dev.getName().set( "Caleb Cushing" );
                            dev.getEmail().set( "xenoterracide@gmail.com" );
                            dev.getUrl().set( "https://xenoterracide.com" );
                        } );
                    } );
                    pub.from( project.getComponents().getAt( "java" ) );
                    pub.artifact( sources.get() );
                    pub.artifact( javadocJar.get() );
                } );
            } );
        });
    }
}
