plugins {
    idea
    `java-library`
    `java-gradle-plugin`
    id("com.gradle.plugin-publish").version("0.10.0")
    id("com.xenoterracide.gradle.sem-ver").version("0.8.4")
}
group = "com.xenoterracide"

repositories {
    maven("https://plugins.gradle.org/m2/")
    mavenCentral()
}

dependencies {
    implementation("gradle.plugin.com.xenoterracide:gradle-junit:[0.1.1,0.2)")
    implementation("gradle.plugin.com.xenoterracide:gradle-idea:[0.1.2,0.2)")
    implementation("gradle.plugin.com.xenoterracide:gradle-semver:[0.8.4,0.9)")
    implementation("gradle.plugin.com.xenoterracide:gradle-mirror:[0.2.1,0.3)")
    implementation("gradle.plugin.com.xenoterracide:gradle-bom:[0.8.2,0.9)")
    implementation("gradle.plugin.com.xenoterracide:gradle-kotlin:[0.8.1,0.9)")
    implementation("org.jetbrains.dokka:dokka-gradle-plugin:0.9.17")
    testImplementation("org.assertj:assertj-core:[3.9,4.0)")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.+")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.+")
}

dependencyLocking {
    lockAllConfigurations()
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
pluginBundle {
    // These settings are set for the whole plugin bundle
    vcsUrl = "https://bitbucket.org/xenoterracide/gradle-bundle-kotlin-lib"
    website = vcsUrl
    plugins {
        create("bundle.kotlin.lib") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Default Kotlin Library Settings"
            description = "Xeno's Default Kotlin Library Settings"
            tags = listOf("default", "bundle", "kotlin")
        }
    }
}

